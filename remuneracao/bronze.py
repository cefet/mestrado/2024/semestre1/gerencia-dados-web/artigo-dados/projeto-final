"""
Separação dos blocos de informações dentro das planilhas:
carreira e cargos, tabela de dados e cabeçalhos, notas sobre regulação.
"""

import numpy as np
import pandas as pd
from pathlib import Path
from typing import Iterator
import sys


def extract(filename: str) -> Iterator[pd.DataFrame]:
    """
    Extrai os blocos de informações da planilha.

    Os blocos são separados por linhas vazias dentro da planilha.
    Em geral são relativos às carreira e cargos, tabela de dados
    e cabeçalhos, notas sobre regulação.

    Args:
        filename (str): Caminho da planilha.

    Yields:
        Iterator[pd.DataFrame]: Blocos de informações.
    """
    data = pd.read_excel(filename, header=None)

    # Replace empty string to Not a Number (NaN)
    data.replace(r'^\s*$', np.nan, regex=True, inplace=True)

    # Empty rows
    empty = data.isna().all(axis='columns')

    # Dataframe splitted by empty rows
    tables = [group for key, group in data[~empty].groupby(empty.cumsum())]

    for table in tables:
        table.dropna(axis='columns', how='all', inplace=True)
        table.reset_index(inplace=True)
        table.drop(columns='index', inplace=True)

        yield table


def save(blocks: Iterator[pd.DataFrame], output_directory: str, base: str):
    """
    Salva os blocos de informações em arquivos CSV.

    Separador de colunas como ponto e vígula e separador de decimal com ponto.

    Args:
        blocks (Iterator[pd.DataFrame]): Blocos de informações.
        output_directory (str): Diretório de saída.
        base (str): Nome base para os arquivos CSV.
    """
    for block_index, block in enumerate(blocks):
        filename = Path(output_directory, f'{base}_{block_index}.csv')
        block.to_csv(filename, header=False, index=False, sep=';')


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit(f"Usage: python {sys.argv[0]} [input filename] [output directory]")

    _, input_filename, output_directory = sys.argv

    path = Path(input_filename)

    blocks = extract(path)
    save(blocks, output_directory, base=path.stem)
