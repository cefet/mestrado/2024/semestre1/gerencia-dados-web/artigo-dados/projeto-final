
"""
Caminhos para criação dos diretório de dados.
"""

import pandas as pd
from pathlib import Path
import regex as re


def check(title: str, words: list[str]) -> bool:
    """
    Check all words in title.

    Args:
        title (str): Title to check.
        words (list[str]): List of words to check.

    Returns:
        bool: True if all words are in title. False if any is not in title.
    """
    return all(True if word in title else False for word in words)


def get_directories(index: pd.DataFrame):
    """
    Gets diretories between part II and part III.

    Args:
        index (pd.DataFrame): Index of jobs of public federal civil servants.
    """
    rows = index.iterrows()
    start = ['II', 'CARREIRAS', 'CARGOS']
    end = ['III', 'CARGOS', 'COMISSÃO', 'GRATIFICAÇÕES']

    for row_id, row in rows:
        if check(row.title, start):
            break

    for row_id, row in rows:
        if check(row.title, end):
            break

        if pd.isna(row.page):
            # root titles
            titles = re.split(r'^[0-9]+\.\s', row.title)

            if len(titles) > 1:
                title = titles[1].replace('/', '-')
                base = Path(title)
                root = Path(title)
            else:
                title = titles[0].replace('/', '-')
                root = base.joinpath(title)

            continue

        title = row.title.replace('/', '-')
        yield root.joinpath(title)


def make_directories(filename: str):
    """
    Makes directories based on index file.

    Args:
        filename (str): Index filename.
    """
    index = pd.read_csv(filename, sep=';', names=['title', 'page'])
    index.dropna(how='all', inplace=True)
    index.reset_index(inplace=True)

    results = get_directories(index)

    medallions = ['bruto', 'bronze', 'prata', 'ouro']

    for result in results:
        for medallion in medallions:
            base = Path(f'data/{medallion}/II - CARREIRAS E CARGOS')
            path = base.joinpath(result)
            path.mkdir(parents=True, exist_ok=True)

    print('Creation of directories is done!')


if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit(f"Usage: python {sys.argv[0]} [index filename]")

    _, filename = sys.argv


    make_directories(filename)
