"""
Mapeamento dos dados originais na estrutura
de diretórios de acordo com o índice.
"""

from pathlib import Path
from tempfile import TemporaryDirectory
import sys
from zipfile import ZipFile


def update_mapping(mapping: dict[str, str], base: str):
    for key, value in mapping.items():
        newkey = f"{base}{key}"

        yield newkey, value


def extract(
    filename: str,
    mapping: dict[str, str],
):
    """
    Extrai os arquivos para nomes definidos no mapeamento.

    Args:
        filename (str): Caminho do arquivo compactado.
        mapping (dict[str, str]): Mapeamento para um caminho mais expressivo.
    """

    with ZipFile(filename) as zipped:
        with TemporaryDirectory(dir=".cached") as temp_dir:
            base, *names = zipped.namelist()
            mapping = dict(update_mapping(mapping, base))

            for name in names:
                if Path(name).suffix == ".ods":
                    try:
                        output = Path(mapping[name])
                    except KeyError:
                        # print(f'No spreadsheet: {name}')
                        continue

                    extracted = zipped.extract(member=name, path=temp_dir)
                    result = Path(extracted).replace(output)
                    print(result)


def create_mapping(base: str, output: str) -> dict[str, str]:
    """
    Cria mapeamento do nome original para um nome mais expressivo.

    Args:
        base (str): Diretório base de saída.
        output (str): Nome do arquivo de saída.

    Returns:
        dict[str, str]: Mapeamento para um caminho mais expressivo.
    """

    mapping = {
        "22. Doc Cargos/Mag Super/Carre MgSup/CarrMSup DE NS.ods": f"{base}/II - CARREIRAS E CARGOS/DOCENTE/Magistério Superior/Cargo: Professor do Magistério Superior, de que trata a Lei nº 7.596-1987 - Dedicação Exclusiva/{output}.ods",
        "26. FIOCRUZ Cargos/Carreiras FIOCRUZ/Pesquisador Fiocruz NS.ods": f"{base}/II - CARREIRAS E CARGOS/Fundação Oswaldo Cruz - FIOCRUZ/Pesquisador em Saúde Pública - FIOCRUZ - NS/{output}.ods",
        "45. Plano Especial Polícia Federal Cargos/Plano Esp CPF NS.ods": f"{base}/II - CARREIRAS E CARGOS/Plano Especial de Cargos do Departamento de Polícia Federal/Cargos de Nível Superior do Plano Especial de Cargos do Departamento de Polícia Federal - NS/{output}.ods",
    }

    return mapping


if __name__ == "__main__":
    if len(sys.argv) < 4:
        sys.exit(f"Usage: python {sys.argv[0]} [input file] [output dir] [output file]")

    _, input_filename, output_directory, output_filename = sys.argv

    mapping = create_mapping(output_directory, output_filename)

    extract(input_filename, mapping)
