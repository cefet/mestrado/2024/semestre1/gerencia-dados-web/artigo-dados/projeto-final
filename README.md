# Artigo de dados: Remuneração dos servidores públicos federais do Brasil - Comparação entre carreiras de docentes, saúde e polı́cia em 2023

Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)

Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Disciplina: Gerência de Dados da Web

Professores: Thiago Magela Rodrigues Dias e Gray Farias Moita

Estudante: André Almeida Rocha


Projeto final: artigo de dados


## Dados
![Arquitetura medalhão](images/medalhao.png)

Os dados estão disponíveis através de uma arquitetura medalhão separadas em camadas bruto, bronze, prata e ouro.

Para cada camada foi gerada uma árvore de diretórios a partir do arquivo de índice de carreira e cargos, usando o script [remuneracao/diretorios.py](remuneracao/diretorios.py).

### Bruto
Dados originais descompatados dentro da árvore de diretório, em formato ODS. Foi usado o script [remuneracao/bruto.py](remuneracao/bruto.py)

### Bronze
Blocos de informações disponíveis nas planilhas, em formato CSV. Foi usado o script [remuneracao/bronze.py](remuneracao/bronze.py)

Em geral são relativos às carreira e cargos, tabela de dados e cabeçalhos, notas sobre regulação. Esses blocos são separados por linhas vazias dentro da planilha.

### Prata
Tabelas de dados com cabeçalhos processados manualmente. Devido a grande variabilidade de formato de tabelas e cabeçalhos o processamento automatizado poderia ser feito individualmente para cada arquivo disponível na camada bronze.

### Ouro
Processamento dos dados da camada prata dentro de um contexto específico, portanto é nessa que ficarão os dados dos trabalhos futuros com o conjunto de dados proposto nesse artigo de dados.


## Licença

[Creative Commons Atribuição 4.0 Internacional](https://creativecommons.org/licenses/by/4.0/)


Você tem o direito de:

    Compartilhar — copiar e redistribuir o material em qualquer suporte ou formato para qualquer fim, mesmo que comercial.
    Adaptar — remixar, transformar, e criar a partir do material para qualquer fim, mesmo que comercial.
    O licenciante não pode revogar estes direitos desde que você respeite os termos da licença.

De acordo com os termos seguintes:

    Atribuição — Você deve dar o crédito apropriado , prover um link para a licença e indicar se mudanças foram feitas . Você deve fazê-lo em qualquer circunstância razoável, mas de nenhuma maneira que sugira que o licenciante apoia você ou o seu uso.
    Sem restrições adicionais — Você não pode aplicar termos jurídicos ou medidas de caráter tecnológico que restrinjam legalmente outros de fazerem algo que a licença permita.

Avisos:

Você não tem de cumprir com os termos da licença relativamente a elementos do material que estejam no domínio público ou cuja utilização seja permitida por uma exceção ou limitação que seja aplicável.

Não são dadas quaisquer garantias. A licença pode não lhe dar todas as autorizações necessárias para o uso pretendido. Por exemplo, outros direitos, tais como direitos de imagem, de privacidade ou direitos morais , podem limitar o uso do material.
